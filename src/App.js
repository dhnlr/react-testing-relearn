import { Routes, Route } from "react-router-dom";

import Home from "./Home";
import Fetch from "./Fetch";

function App() {
  return (
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="fetch" element={<Fetch />} />
        <Route
          path="*"
          element={
            <div>
              <h1>Not found</h1>
            </div>
          }
        />
      </Routes>
  );
}

export default App;
