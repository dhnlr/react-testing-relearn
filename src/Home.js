import { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import logo from "./logo.svg";
import "./Home.css";

function Home() {
  const ALLOWED_KEYS = ["ArrowUp", "ArrowDown", "ArrowLeft", "ArrowRight", "?"];
  const [counter, setCounter] = useState(0);
  const [pressedKeys, setPressedKeys] = useState([]);
  const [showShortcut, setShowShortcut] = useState(false);

  useEffect(() => {
    const onKeyDown = ({ key }) => {
      if (ALLOWED_KEYS.includes(key) && !pressedKeys.includes(key)) {
        setPressedKeys((previousPressedKeys) => [...previousPressedKeys, key]);
      }
    };

    const onKeyUp = ({ key }) => {
      if (ALLOWED_KEYS.includes(key)) {
        setPressedKeys((previousPressedKeys) =>
          previousPressedKeys.filter((k) => k !== key)
        );
        handleKeyboard(key);
      }
    };

    document.addEventListener("keydown", onKeyDown);
    document.addEventListener("keyup", onKeyUp);

    return () => {
      document.removeEventListener("keydown", onKeyDown);
      document.removeEventListener("keyup", onKeyUp);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [counter]);

  const handleAddCounter = () => {
    setCounter(counter + 1);
  };

  const handleMinCounter = () => {
    if (counter > 0) setCounter(counter - 1);
  };

  const handleResetCounter = () => {
    setCounter(0);
  };

  const handleKeyboard = (key) => {
    switch (key) {
      case "ArrowUp":
        handleAddCounter();
        break;
      case "ArrowDown":
        handleMinCounter();
        break;
      case "ArrowLeft":
        handleResetCounter();
        break;
      case "ArrowRight":
        setCounter(counter + 10);
        break;
      case "?":
        setShowShortcut(true);
        break;
      default:
        break;
    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <main>
          <h1>{counter}</h1>
          <button onClick={handleAddCounter}>+</button>
          <button onClick={handleMinCounter}>-</button>
          <button onClick={handleResetCounter}>Reset</button>
        </main>
        {showShortcut && (
          <div>
            <table>
              <thead>
                <tr>
                  <th>Event</th>
                  <th>Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>ArrowUp</td>
                  <td>Add 1 to counter</td>
                </tr>
                <tr>
                  <td>ArrowDown</td>
                  <td>Minus 1 to counter</td>
                </tr>
                <tr>
                  <td>ArrowLeft</td>
                  <td>Reset counter to zero</td>
                </tr>
                <tr>
                  <td>ArrowRight</td>
                  <td>Add 10 to counter</td>
                </tr>
                <tr>
                  <td>?</td>
                  <td>Show shortcut list</td>
                </tr>
              </tbody>
            </table>
          </div>
        )}
        <Link to="/fetch">Fetch</Link>
      </header>
    </div>
  );
}

export default Home;
