import { render as rtlRender, screen } from "@testing-library/react";
import { BrowserRouter } from 'react-router-dom'
import App from "../App";

const render = (ui, {route = '/'} = {}) => {
  window.history.pushState({}, 'Test page', route)
  return rtlRender(ui, {wrapper: BrowserRouter})
}

test("renders apps", () => {
  render(<App />);
  expect(screen.getByText(/learn react/i)).toBeInTheDocument()
});

test("render not found", () => {
    render(<App />, {route: "/route-not-found"})
    expect(screen.getByText(/not found/i)).toBeInTheDocument()
})
