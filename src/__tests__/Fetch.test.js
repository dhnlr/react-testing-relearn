import { render as rtlRender, screen, fireEvent } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import { rest } from "msw";
import { setupServer } from "msw/node";
import "@testing-library/jest-dom/extend-expect";
import Fetch from "../Fetch";

const server = setupServer(
  rest.get("https://pokeapi.co/api/v2/pokemon/1", (req, res, ctx) => {
    return res(
      ctx.json({
        count: 1118,
        next: "https://pokeapi.co/api/v2/pokemon?offset=1&limit=1",
        previous: null,
        name: "bulbasaur",
        location_area_encounters: "https://pokeapi.co/api/v2/pokemon/1/",
      })
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const render = (ui, { route = "/" } = {}) => {
  window.history.pushState({}, "Test page", route);
  return rtlRender(ui, { wrapper: BrowserRouter });
};

test("load pokemon api", async () => {
  render(<Fetch />);
  expect(await screen.findByRole("heading", { name: "bulbasaur" })).toBeInTheDocument();
  expect(await screen.findByText("Details")).toHaveAttribute(
    "href",
    "https://pokeapi.co/api/v2/pokemon/1/"
  );
});
