import { render as rtlRender, screen, fireEvent } from "@testing-library/react";
import { BrowserRouter } from 'react-router-dom'
import Home from "../Home";

const render = (ui, {route = '/'} = {}) => {
  window.history.pushState({}, 'Test page', route)
  return rtlRender(ui, {wrapper: BrowserRouter})
}

test("renders apps", () => {
  render(<Home />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});

test("default counter state", () => {
  render(<Home />);
  const counter = screen.getByText(0);
  expect(counter).toBeInTheDocument();
});

test("add counter button", () => {
  render(<Home />);
  fireEvent.click(screen.getByText("+"));
  const counter = screen.getByText(1);
  expect(counter).toBeInTheDocument();
});

test("minus counter button", () => {
  render(<Home />);
  fireEvent.click(screen.getByText("+"));
  fireEvent.click(screen.getByText("-"));
  const counter = screen.getByText(0);
  expect(counter).toBeInTheDocument();
});

test("reset counter button", () => {
  render(<Home />);
  fireEvent.click(screen.getByText("+"));
  fireEvent.click(screen.getByText("Reset"));
  const counter = screen.getByText(0);
  expect(counter).toBeInTheDocument();
});

test("Arrow up keyup event", () => {
  render(<Home />);
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowUp"})
  const counter = screen.getByText(1);
  expect(counter).toBeInTheDocument();
})

test("Arrow down keyup event", () => {
  render(<Home />);
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowUp"})
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowDown"})
  const counter = screen.getByText(0);
  expect(counter).toBeInTheDocument();
})

test("Arrow left keyup event", () => {
  render(<Home />);
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowUp"})
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowLeft"})
  const counter = screen.getByText(0);
  expect(counter).toBeInTheDocument();
})

test("Arrow right keyup event", () => {
  render(<Home />);
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "ArrowRight"})
  const counter = screen.getByText(10);
  expect(counter).toBeInTheDocument();
})

test("Question mark keyup event", () => {
  render(<Home />)
  fireEvent.keyUp(screen.getByText(/learn react/i), {key: "?"})
  const counter = screen.getByText(/shortcut list/i);
  expect(counter).toBeInTheDocument();
})