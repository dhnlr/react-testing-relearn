import axios from "axios";
import { useEffect, useState } from "react";

function Fetch() {
  const [pokemon, setPokemon] = useState(null);

  useEffect(() => {
    axios.get("https://pokeapi.co/api/v2/pokemon/1").then(({ data }) => {
      setPokemon(data);
    });
  }, []);

  return (
    <div>
      {pokemon && (
        <div>
          <h1>{pokemon.name}</h1>
          <a href={pokemon.location_area_encounters}>Details</a>
        </div>
      )}
      {!pokemon && <p>No data</p>}
    </div>
  );
}

export default Fetch;
